 <?php
	
    function addSong(){
        if(empty($_POST["title"]) || trim($_POST["title"])==''){
            $errorArr[]="title";
        };
        if(!isset($_POST["geshou"])||trim($_POST["geshou"])==''){
            $errorArr[]="geshou";
        };
        if(!isset($_POST["zhuanji"])||trim($_POST["zhuanji"])==''){
            $errorArr[]="zhuanji";
        };
        if(!isset($_FILES['source'])||$_FILES['source']['error']!=0){
            $errorArr[]="source";
        };

        print_r($errorArr);
        echo  $errorArr["title"];
        if(isset($errorArr)){
            $GLOBALS['err_arr']=$errorArr;

            return;
        }

        /*数据存储*/
        /*1、实现文件上传*/
        move_uploaded_file($_FILES['source']['tmp_name'],'./mp3/' . $_FILES['source']['name']);
        /*2、将数据写入到json中*/
        $data=file_get_contents("music.json");
        /*2.1 将json数据转换成php数组*/
        $dataArr=json_decode($data,true);
        /*2.2、获取表单输入的数据存至一个数组中*/
        $newSong=array(
            "id"=>count($dataArr)==0?1:$dataArr[count($dataArr)-1]['id']+1,
            "title"=>$_POST['title'],
            "singer"=>$_POST['geshou'],
            "album"=>$_POST['zhuanji'],
            "src"=>'./mp3/'.$_FILES['source']['name']
            );
        print_r($newSong);
        /* 2.3、将数据添加至数组中 */
        $dataArr[]=$newSong;

        /*3、将输入的文件添加json文件中*/
        file_put_contents("music.json",json_encode( $dataArr));

        /*4、实现页面跳转至列表页*/
        echo '<script>location.href="list.php";</script>';
    }

   if($_SERVER['REQUEST_METHOD']=="POST") {
       addSong();
   }else{
       $GLOBALS['err_arr']=[];
   }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="bootstrap.css">
    <style>
        .showInfo{
            display:block;
        }
    </style>
</head>
<body>
	<div class="container">
		<h1 class=" display-3 py-3">音乐上传</h1>
		<hr>
		<!-- 表单结构： -->
		<form action="<?php echo $_SERVER["PHP_SELF"]?>" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="title">标题</label>
				<!-- 下面的所有 is-invalid 均为要判断的类名 -->
				<!-- 标题输入框是否需要添加is-invalid类名，需要取决于$error_arr中是否含有'title'这个值 -->
				<input type="text" class="form-control " id="title" name="title">
				<small class="invalid-feedback <?php echo  in_array('title',  $GLOBALS['err_arr']) ? 'showInfo' : '' ?>">请输入标题</small>
			</div>
			<div class="form-group">
				<label for="geshou">歌手</label>
				<input type="text" class="form-control " id="geshou" name="geshou">
				<small class="invalid-feedback <?php echo  in_array('geshou', $GLOBALS['err_arr']) ? 'showInfo' : '' ?>">请输入歌手的名称</small>
			</div>
			<div class="form-group">
				<label for="zhuanji">专辑</label>
				<input type="text" class="form-control " id="zhuanji" name="zhuanji">
				<small class="invalid-feedback <?php echo  in_array('zhuanji', $GLOBALS['err_arr']) ? 'showInfo' : '' ?>">请输入专辑名称</small>
			</div>
			<div class="form-group">
				<label for="source">资源文件</label>
				<!-- accept 用于设置可以接受的文件类型，可以使用MIMEtype,也可以使用后缀名，使用逗号连接 -->
				<input type="file" class="form-control " id="source" name="source" accept=".mp3,.png">
				<small class="invalid-feedback <?php echo  in_array('source', $GLOBALS['err_arr']) ? 'showInfo' : '' ?>">请上传文件</small>
			</div>
			<button class="btn btn-primary btn-block">提交</button>
		</form>
	</div>
</body>
</html>