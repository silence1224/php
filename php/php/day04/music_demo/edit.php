<?php
    function  edit(){
        if(!isset($_POST['title'])||trim($_POST["title"])==''){
            $errorArr[] = "title";
        };
        if(!isset($_POST["geshou"])||trim($_POST["geshou"])==''){
            $errorArr[] = "geshou";
        }
        if(!isset($_POST['zhuanji'])||trim($_POST["zhuanji"])==''){
            $errorArr[] = "zhuanji";
        }

        // 对于文件的判断
        // 因为我们做的是修改操作：修改操作不能强迫用户去修改所有的数据，如果用户如果没有输入值，说明他想使用之前的数据
        // 1.如果用户没有选择文件，说明不想修改这个文件的值，那么也不应该给出相应的错误提示
        // 2.如果用户传递的文件，但是上传失败了，才需要给出相应的提示
        // 判断有没有上传东西	上传是否失败，0代表成功，其它的代表失败
        /*只需要提示上传了文件且上传失败了*/

        /*if(!empty($_FILES) && $_FILES['source']['error']!==0){
            $errorArr[] = "source";
        }*/


        // echo  "-----";
        // echo  "<pre>";
        // print_r($_FILES);
        // var_dump(isset($_FILES['source']['name']));
        // echo  "-----";

        if(!empty(['source']['name']) && $_FILES['source']['error']!==0){
            $errorArr[] = "source";
        };

        /*整体判断数组是否有值*/
        if(isset($errorArr) && count($errorArr)!=0){
            $GLOBALS['errorArr']=$errorArr;
            echo "输入有误";
            return;
        };

        /*实现数据的修改*/
        echo  "<pre>";
        print_r($_FILES);
        if(isset($_FILES['source']['name']) && $_FILES['source']['error']!=0){
            move_uploaded_file($_FILES['source']['tmp_name'],'./mp3/'.$_FILES['source']['name']);
        };

        /*1、获取id*/
        $id=$_POST['id'];
        /*2、获取数据*/
        $data=file_get_contents('music.json');
        /*3、将数据转换成数组*/
        $dataArr=json_decode($data,true);

       /*  echo  "<pre>";
        print_r($dataArr); */
        //echo $id,'=======';
        /*4、对数组进行遍历*/
        foreach($dataArr as $key=>$value){
            //echo $value['id'],'------';
            /*4、1匹配id相同的将数据进行更改*/
            if($value['id']==$id){
                //echo $_POST['title'];
                //echo $_POST['geshou'];
                // array(
                //     "title"=>"张三之歌"
                // );


                $dataArr[$key]['title']=$_POST['title'];
                $dataArr[$key]['singer']=$_POST['geshou'];
                $dataArr[$key]['album']=$_POST['zhuanji'];
                /*4。2、当上传了文件且存在错误值等于0说明上传成功就更改*/
                if(isset($_FILES['source']['name']) && $_FILES['source']['error']==0){
                    $dataArr[$key]['src']='./mp3/'.$_FILES['source']['name'];
                };
                break;
            }
        };
        /*5、将数据写入*/
        //print_r($dataArr);
        file_put_contents('music.json',json_encode($dataArr));
        /*6、实现跳转*/
        //echo '<script>location.href="list.php";</script>';


    }
	
    if($_SERVER['REQUEST_METHOD']==='GET'){
        /*1、接受id*/
		$id=$_GET['id'];
		/* 2、获取数据 music.json */
		$data=file_get_contents('music.json');
		/* 3、将数据转换成数组 */
		$dataArr=json_decode($data,true);
		foreach($dataArr as $key=>$value){
			/* 4、根据传过来的id进行数据匹配 */
			if($value['id']==$id){
				$current=$value;
			}
		};
	}else if($_SERVER['REQUEST_METHOD']==='POST'){
        edit();
        print_r($_POST);
        /*1、接受id*/
        $id=$_POST['id'];
        /* 2、获取数据 music.json */
        $data=file_get_contents('music.json');
        /* 3、将数据转换成数组 */
        $dataArr=json_decode($data,true);
        foreach($dataArr as $key=>$value){
            /* 4、根据传过来的id进行数据匹配 */
            if($value['id']==$id){
                $current=$value;
            }
        };
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="bootstrap.css">
    <style>
        .showInfo{
            display:block;
        }
    </style>
</head>
<body>
	<div class="container">
		<h1 class=" display-3 py-3">音乐上传</h1>
		<hr>
		<!-- 表单结构： -->
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo  $current['id']  ?>"/>
            <div class="form-group">
				<label for="title">标题</label>
				<!-- 下面的所有 is-invalid 均为要判断的类名 -->
				<!-- 标题输入框是否需要添加is-invalid类名，需要取决于$error_arr中是否含有'title'这个值 -->
				<input type="text" class="form-control " value="<?php echo  $current['title']  ?>" id="title" name="title">
				<small class="invalid-feedback <?php echo  in_array('title',$GLOBALS['errorArr'])?'showInfo':'' ?>">请输入标题</small>
			</div>
			<div class="form-group">
				<label for="geshou">歌手</label>
				<input type="text" class="form-control " value="<?php echo  $current['singer']  ?>"  id="geshou" name="geshou">
				<small class="invalid-feedback <?php echo  in_array('geshou',$GLOBALS['errorArr'])?'showInfo':'' ?>">请输入歌手的名称</small>
			</div>
			<div class="form-group">
				<label for="zhuanji">专辑</label>
				<input type="text" class="form-control " value="<?php echo  $current['album']  ?>" id="zhuanji" name="zhuanji">
				<small class="invalid-feedback <?php echo  in_array('zhuanji',$GLOBALS['errorArr'])?'showInfo':'' ?>">请输入专辑名称</small>
			</div>
			<div class="form-group">
				<label for="source">资源文件</label>
				<!-- accept 用于设置可以接受的文件类型，可以使用MIMEtype,也可以使用后缀名，使用逗号连接 -->
				<input type="file" class="form-control "  id="source" name="source" accept=".mp3,.png">
				<small class="invalid-feedback <?php echo  in_array('source',$GLOBALS['errorArr'])?'showInfo':'' ?>">请上传文件</small>
			</div>
			<button class="btn btn-primary btn-block">提交</button>
		</form>
	</div>
</body>
</html>