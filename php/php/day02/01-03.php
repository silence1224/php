<?php

header("Content-Type:text/html;charset=utf-8");
$arr_a=[1,2,3];

echo  "<pre>";
print_r($arr_a);
echo  "+++++++++++++++";

$arr_b=array(
    "name"=>"zs",
    "age"=>23,
    "gender"=>"男"
);

print_r($arr_b);

    $arr=[1,2,3,true,"abc"];
    echo $arr[2],"-----";

    for($i=0;$i<count($arr);$i++){
        echo $arr[$i].'------';
    }

echo "<pre>";
echo  '<hr>';
    $aa=array(
        "name"=>"zs",
        "age"=>"22",
        "gender"=>"男"
    );
    echo  $aa['name'].'========';
    foreach($aa  as  $key=>$value){
        echo  $value;
    };/*  */

    /*索引数组*/
echo "<pre>";
echo  '<hr>';
    $bb=array(1,2,3,4);
    var_dump($bb);

    /*关联数组*/
echo "<pre>";
echo  '<hr>';
    $cc=array("key1"=>"1","key2"=>"2","key3"=>"3");
    var_dump($cc);

    /*混合数组*/
echo "<pre>";
echo  '<hr>';
    $dd=array(1,2,3,"key1"=>"1a","key2"=>"2a",4);
    var_dump($dd);

    /*通过[]创建数组*/
echo "<pre>";
echo  '<hr>';
    $ee[]=1;
    $ee[]=2;
    $ee["name"]="jack";
    $ee[]=3;
    print_r($ee);

/*数组常用函数*/
echo "<pre>";
echo  '<hr>';
echo"========";
    $ff=[1,2,3,4,5];
    unset($ff[2]);
    print_r($ff);
echo"========";



/*二维数组*/
echo "<pre>";
echo  '<hr>';
$gg=array(
    1,
    2,
    4,
    array("a","b","c")
);
print_r($gg);
echo  '<hr>';



$hh=array(
    "first"=>array(
        "name"=>"jack",
        "age"=>"22"
    ),
    array(
        "name2"=>"rose",
        "age2"=>"20"
    ),
);
print_r($hh);

/*遍历二维数组*/
echo  '<hr>';
foreach($hh as  $key => $value){
    //print_r($value);
    foreach($value as  $subkey => $subvalue){
        echo  "====";
        //print_r($subkey);
        echo  "====";
        //print_r($subvalue);
        echo  $subkey.':'.$subvalue.'<br>';    //name:jack
    }
}

?>