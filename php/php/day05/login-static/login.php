<?php 

///* 1.判断数据输入是否存在 */
//    function  login(){
//        if(!isset($_POST['username']) || $_POST['username']==''){
//            $GLOBALS['erroror'] = '请输入用户名';
//            return;
//        };
//        if(!isset($_POST['password']) || $_POST['password']==''){
//            $GLOBALS['erroror'] = '请输入密码';
//            return;
//        };
//
//        /* 2.获取表单数据 */
//        $username=$_POST['username'];
//        $password=$_POST['password'];
//
//        /* 3.读取文件进行判断是否有一致的 */
//        $data_arr=json_decode(file_get_contents('users.json'),true);
//        foreach($data_arr as  $key => $value){
//            if($value['username']==$username){
//                $user=$value;
//                break;
//            };
//        };
//
//        if(!isset($user)){
//          $GLOBALS['erroror'] = '用户名不存在';
//          return;
//        };
//        if($user['password']!= $password){
//          $GLOBALS['erroror'] = '密码不正确';
//          return;
//        };
//
//
//      /*   setcookie('islogin',true); */
//
//        session_start();
//        $_SESSION['user'] = array(
//            'username'=>"username",
//            "password"=>"password",
//            "islogin"=>'yes'
//        );
//        header("Location:./main.php");
//    };
//
//
//
//
//    if($_SERVER["REQUEST_METHOD"]=='POST'){
//        login();
//    }


/*1、判断是否死post请求*/
/*2、是post请求校验表单数据*/
/*3、获取表单数据*/
/*4、读取文件，将文件转换成数组进行遍历*/
/*5、遍历的每一项和数去的表单数据进行匹配，并记录匹配成功的那一项进行判断*/
/*6、存储cookie*/
/*7、成功后实现页面跳转*/


/*1、判断是否死post请求*/
if($_SERVER['REQUEST_METHOD']==='POST'){
    login();
};

function login(){
    /*2、是post请求校验表单数据*/
    if(!isset($_POST['username'])||trim($_POST['username'])==''){
        $GLOBALS['error']='用户名输入有误';
        return;
    };
    if(!isset($_POST['password'])||trim($_POST['password'])==''){
        $GLOBALS['error']='密码名输入有误';
        return;
    };
    /*3、获取表单数据*/
    $username=$_POST["username"];
    $password=$_POST["password"];
    /*4、读取文件，将文件转换成数组进行遍历*/
    $data=file_get_contents("./users.json");
    $dataArr=json_decode($data,true);
    foreach ($dataArr as  $key=>$value){
        /*5、遍历的每一项和数去的表单数据进行匹配，并记录匹配成功的那一项*/
        if($value["username"]==$username){
            $user=$value;
            break;
        }
    };

    if(!isset($user)){
        $GLOBALS['error']='用户不存在';
        return;
    };
    /*用户名存在密码不存在*/
    if(!isset($user['password'])){
        $GLOBALS['error']="用户不存在";
        return;
    };
    /*6、存储cookie*/
   /*  setcookie("isLogin",true); */

    /*session实现*/
    session_start();
    $_SESSION["user"]= array(
        "username"=>$_POST["username"],
        "password"=>$_POST["password"],
        "isLogin"=>"yes"
    );

    /*7、成功后实现页面跳转*/
    header("Location:./main.php");






}



?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>登录</title>
  <link rel="stylesheet" href="bootstrap.css">
  <style>
    body {
      background-color: #f8f9fb;
    }

    .login-form {
      width: 360px;
      margin: 100px auto;
      padding: 30px 20px;
      background-color: #fff;
      border: 1px solid #eee;
    }

    .login-form h1 {
      font-size: 30px;
      margin-bottom: 20px;
    }
  </style>
</head>
<body>
  <form class="login-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
    <h1>登录</h1>   
    <!-- 下面的错误提示信息结构 需要在有错误信息的时候进行显示 -->
    <?php
      if(isset($GLOBALS["error"])){ ?>
        <div class="alert alert-danger" role="alert"><?php echo $GLOBALS["error"]; ?></div>
      <?php }
    ?>
    <div class="form-group">
      <label for="username">用户名</label>
      <input type="text" class="form-control" id="username" name="username"">
    </div>
    <div class="form-group">
      <label for="password">密码</label>
      <input type="password" class="form-control" id="password" name="password">
    </div>
    <button type="submit" class="btn btn-primary btn-block">登录</button>
  </form>
</body>
</html>