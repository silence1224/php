<?php
    /*1、设置响应头
      2、建立数据库连接
      3、设置编码格式
      4、判断连接状态
      5、创建查询语句
      6、判断结果集
    */
     /* 1、设置响应头*/
     header("Content-Type:text/html;charset=utf8");
     /* 2、建立数据库连接*/
     $conn=mysqli_connect("localhost","root","root","my");
     /* 3、设置编码格式*/
     mysqli_query($conn,"set  name  utf-8");
      /*4、判断连接状态*/
      if(!$conn){
          die("数据库连接失败");
      }
      /*5、创建查询语句*/
      $sql="select  *  from  mytable";
      $result=mysqli_query($conn,$sql);
      /*6、判断结果集*/
      if(!$result){
          echo  "查询失败";
      }elseif (mysqli_num_rows($result)==0){
          die("结果集为空");
      }else{
          //echo  "<pre>";
          //获取数据的函数：下面这几个函数有一个共同点，就是只能读取这一行，但是读取完这一行，移到下一行，
          //如果没有读取到任何数据，则返回null

          //var_dump($result);
          //print_r(mysqli_fetch_array($result)); //提取数据生成一个数组，同时生成一个索引数组和一个关联数组
          //print_r(mysqli_fetch_assoc($result)); //提取数据生成一个数组:将数组生成一个关联数组
          //print_r(mysqli_fetch_row($result));   //提取数据生成一个数组:将数组生成一个索引数组

          /*
           * 打印类型转换
           * mysqli_fetch_array($result,返回内容形式MYSQL_ASSOC||MYSQL_NUM||MYSQL_BOTH);
           * */

//           print_r(mysqli_fetch_array($result,MYSQL_NUM));
//           print_r(mysqli_fetch_array($result,MYSQL_NUM));
//          var_dump(mysqli_fetch_array($result,MYSQL_NUM));
          while($row=mysqli_fetch_array($result,MYSQL_ASSOC)){
              $res[]=$row;
          };
          print_r($res);

      }



?>