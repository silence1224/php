<?php

// 封装增加删除和修改操作
    function  opt($sql){
        /*1、创建连接池*/
        $conn=mysqli_connect("localhost","root","root","my");
        /*2、判断sql编码*/
        mysqli_set_charset($conn,"utf8");
        /*3、判断数据库连接是否成功*/
        if(!$conn){
            die('连接失败！');
        };
        /*4、执行mysql查询*/
        $res=mysqli_query($conn,$sql);
        /*5、关闭连接*/
        mysqli_close($conn);
        /*6、返回结果集*/
        return $res;
    };

// 封装查询操作
    function  select($sql){
        /*1、创建连接池*/
        $conn=mysqli_connect("localhost","root","root","my");
        /*2、判断sql编码*/
        mysqli_set_charset($conn,"utf8");
        /*3、判断数据库连接是否成功*/
        if(!$conn){
            die('连接失败！');
        };
        /*4、执行mysql查询*/
        $res=mysqli_query($conn,$sql);

        /*5、判断查询数据*/
        if(!$res){
            echo  "查询失败！";
        }else if(mysqli_num_rows($res)==0){
            echo  "查询数据为空！";
        }else{
            /*while($row = mysqli_fetch_array($res,MYSQLI_ASSOC)){
                $result[]=$row;
            }*/
            while($row = mysqli_fetch_assoc($res)){
                $result[]=$row;
            };
            /*6、关闭连接*/
            mysqli_close($conn);
            /*7、返回结果集*/
            return $result;
        }
        /*6、关闭连接*/
        mysqli_close($conn);
    }
?>