
var $ = {
    getpa:function(data){
        if(data &&  typeof data=="object"){
            var  str="?";
            for(var key in  data){
                str=str+key+"="+data[k]+"&";
            };
            str=str.substr(0,str.length-1);
        }
        return  str;
    },
    ajax:function(option){
        var  type=option.type||"get";
        var  url=option.url||"location.href";
        var  data=this.getpa(option.data)||"";
        var  success=option.success;

        if(type=="get"){
            url+=data;
            data=null;
        };

        /* 创建异步请求对象 */
        var  xhr=new  XMLHttpRequest();
        xhr.open(type,url);
        if(type=="post"){
            xhr.setRequestHeader("Content-Type","application/x-www-form-encoded");
        };
        xhr.send(data);

        xhr.onreadystatechange=function(){
            if(xhr.status==200&&xhr.readyState==4){
                var  rh=xhr.getAllResponseHeaders("Content-Type");
                if(rh.indexOf("xml")!=="-1"){
                    var  result=xhr.responseXML;
                }else if(rh.indexOf("json")!=="-1"){
                    var  result=JSON.parse(xhr.responseText);
                }else{
                    var  result=xhr.responseText;
                }
                 // 接收数据之后，调用回调函数
                success && success(result);
            }
        }
    },
    get:function(option){

    },
    post:function(){

    }
}
