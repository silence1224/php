<?php
    header("Content-Type:text/html;charset=utf8");


if(isset($_GET["id"])) {

    /*1、获取传参id的值*/
    $id = $_GET["id"];
    //echo $id;
    /*2、获取fruit.txt文件中的数据*/
    $str = file_get_contents("./fruit.txt");
    //echo  $str;
    /*3、以换行来分割*/
    $arr = explode("\n", $str); //["1|img/banana1.jpg|香蕉","1|img/banana1.jpg|香蕉"]
    //print_r($arr);
    /*4、遍历数组对每一项以|分割*/
    foreach ($arr as $key => $value) {
        //print_r($value);
        $result[] = explode("|", $value);    //[1,"img/banana1.jpg","香蕉"]
    }
    //print_r($result);
    foreach ($result as $value) {
        if ($value[0] == $id) {
            $data = $value;
            break;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
    <style>
        .container ul > li {
            float: none;
            width: 100%;
            text-align: center;
        }
        .container ul > li img {
            width: auto;
        }
    </style>
</head>
<body>
    <div class="header">
        传智网上水果超市
    </div>
    <div class="container">
        <p>
            <a href="#">水果</a>
            <a href="#">干果</a>
            <a href="#">蔬菜</a>
        </p>
        <ul>
            <li>
                <!--<img src="<?php /*echo  $data[1] */?>" alt="">
                <p><?php /*echo  $data[2] */?></p>-->

                <img src="<?php if(isset($data)) echo $data[1] ?>" alt="">
                <p>这是 <?php if(isset($data)) echo $data[2]?> 商品的详情图</p>
            </li>
        </ul>
    </div>
    <div class="footer">
        传智播客 版权所有
    </div>
</body>
</html>