
<?php
   //print_r($_SERVER['PHP_SELF']);

    print_r($_FILES);
// 在php中，文件上传之后的相关信息都存储在$_FILES中
// Array
// (
//     [myFile] => Array
//         (
//             [name] => 跨域攻击.png:源文件的名称
//             [type] => image/png：源文件的类型
//             [tmp_name] => C:\Windows\phpE412.tmp：这是文件在服务器的临时路径
//             [error] => 0：错误信息，0代表没有错误
//             [size] => 256436：文件的大小
//         )
// )
        move_uploaded_file($_FILES['myFile']['tmp_name'],'./upload/aa.png');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<!-- 1.在php中，上传文件的请求方式必须是post -->
<!-- 2.在上传的时候必须在表单设置enctype属性
    application/x-www-form-urlencoded：将参数编码为键值对的格式，这是标准的编码格式
    (UTF-8 GBK GB2312),它用来处理字符串，它是默认的编码格式
    multipart/form-data:它是专门用来处理特殊数据的，如文件
-->
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
        选择文件：<input type="file" name="myFile" ><br/>
        <input type="submit">
    </form>
</body>
</html>